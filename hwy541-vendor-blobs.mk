VENDOR_FOLDER := vendor/huawei/hwy541

# Widevine
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libdrmntp.so:system/lib/libdrmntp.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libWVphoneAPI.so:system/lib/libWVphoneAPI.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/drm/libdrmomaplugin.so:system/lib/drm/libdrmomaplugin.so \
    $(VENDOR_FOLDER)/proprietary/system/vendor/lib/libdrmdecrypt.so:system/vendor/lib/libdrmdecrypt.so \
    $(VENDOR_FOLDER)/proprietary/system/vendor/lib/libwvdrm_L3.so:system/vendor/lib/libwvdrm_L3.so \
    $(VENDOR_FOLDER)/proprietary/system/vendor/lib/libwvm.so:system/vendor/lib/libwvm.so \
    $(VENDOR_FOLDER)/proprietary/system/vendor/lib/libWVStreamControlAPI_L3.so:system/vendor/lib/libWVStreamControlAPI_L3.so \
    $(VENDOR_FOLDER)/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so:system/vendor/lib/drm/libdrmwvmplugin.so \
    $(VENDOR_FOLDER)/proprietary/system/vendor/lib/mediadrm/libwvdrmengine.so:system/vendor/lib/mediadrm/libwvdrmengine.so

# Kernel Modules
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/modules/autotst.ko:system/lib/modules/autotst.ko \
    $(VENDOR_FOLDER)/proprietary/system/lib/modules/bcmdhd.ko:system/lib/modules/bcmdhd.ko \
    $(VENDOR_FOLDER)/proprietary/system/lib/modules/mali.ko:system/lib/modules/mali.ko \
    $(VENDOR_FOLDER)/proprietary/system/lib/modules/mmc_test.ko:system/lib/modules/mmc_test.ko

# Battery
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/batterysrv:system/bin/batterysrv \
    $(VENDOR_FOLDER)/proprietary/system/bin/charge:system/bin/charge

# HALs
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/audio.primary.sc8830.so:system/lib/hw/audio.primary.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/audio_policy.sc8830.so:system/lib/hw/audio_policy.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/camera.engmode.sc8830.so:system/lib/hw/camera.engmode.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/camera.sc8830.so:system/lib/hw/camera.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/hwcomposer.sc8830.so:system/lib/hw/hwcomposer.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/gralloc.sc8830.so:system/lib/hw/gralloc.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/lights.sc8830.so:system/lib/hw/lights.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/sensors.sc8830.so:system/lib/hw/sensors.sc8830.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/hw/sprd_gsp.sc8830.so:system/lib/hw/sprd_gsp.sc8830.so

# AMap 2D
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libamapv303.so:system/lib/libamapv303.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libamapv303ex.so:system/lib/libamapv303ex.so

# SPRD Camera
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libae.so:system/lib/libae.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libaf.so:system/lib/libaf.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libawb.so:system/lib/libawb.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libface_finder.so:system/lib/libface_finder.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libmorpho_easy_hdr.so:system/lib/libmorpho_easy_hdr.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libmorpho_memory_allocator.so:system/lib/libmorpho_memory_allocator.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libmorpho_panorama_gp.so:system/lib/libmorpho_panorama_gp.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libuvdenoise.so:system/lib/libuvdenoise.so

# Face unlock
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libfrsdk.so:system/lib/libfrsdk.so

# Audio
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libatchannel.so:system/lib/libatchannel.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libaudiopolicy.so:system/lib/libaudiopolicy.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libdumpdata.so:system/lib/libdumpdata.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libnvexchange.so:system/lib/libnvexchange.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libvbeffect.so:system/lib/libvbeffect.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libvbpga.so:system/lib/libvbpga.so

# NV
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/nvitemd:system/bin/nvitemd

# Graphics
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/egl/libGLES_mali.so:system/lib/egl/libGLES_mali.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libboost.so:system/lib/libboost.so

# Huawei
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libservices.huawei.so:system/lib/libservices.huawei.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libsurfaceflinger.huawei.so:system/lib/libsurfaceflinger.huawei.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libIvw30.so:system/lib/libIvw30.so

# GPS
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/GPSenseEngine:system/bin/GPSenseEngine \
    $(VENDOR_FOLDER)/proprietary/system/etc/GPSenseEngine.xml:system/etc/GPSenseEngine.xml \
    $(VENDOR_FOLDER)/proprietary/system/etc/HiMap.db:system/etc/HiMap.db \
    $(VENDOR_FOLDER)/proprietary/system/etc/supl.xml:system/etc/supl.xml \
    $(VENDOR_FOLDER)/proprietary/system/lib/libsprd_agps_agent.so:system/lib/libsprd_agps_agent.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libsupl.so:system/lib/libsupl.so

# RIL
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/modem_control:system/bin/modem_control \
    $(VENDOR_FOLDER)/proprietary/system/bin/modemd:system/bin/modemd \
    $(VENDOR_FOLDER)/proprietary/system/bin/phoneserver:system/bin/phoneserver \
    $(VENDOR_FOLDER)/proprietary/system/bin/rild_sp:system/bin/rild_sp \
    $(VENDOR_FOLDER)/proprietary/system/bin/rilproxyd:system/bin/rilproxyd \
    $(VENDOR_FOLDER)/proprietary/system/lib/libreference-ril_sp.so:system/lib/libreference-ril_sp.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libril_sp.so:system/lib/libril_sp.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/librilproxy.so:system/lib/librilproxy.so

# WCN
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/wcnd:system/bin/wcnd \
    $(VENDOR_FOLDER)/proprietary/system/bin/iwnpi:system/bin/iwnpi \
    $(VENDOR_FOLDER)/proprietary/system/etc/connectivity_calibration.ini:system/etc/connectivity_calibration.ini \
    $(VENDOR_FOLDER)/proprietary/system/etc/connectivity_configure.ini:system/etc/connectivity_configure.ini \
    $(VENDOR_FOLDER)/proprietary/system/lib/libiwnpi.so:system/lib/libiwnpi.so

# Wifi
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/wl:system/bin/wl \
    $(VENDOR_FOLDER)/proprietary/system/vendor/firmware/bcm4343.hcd:system/vendor/firmware/bcm4343.hcd \
    $(VENDOR_FOLDER)/proprietary/system/vendor/firmware/fw_bcmdhd.bin:system/vendor/firmware/fw_bcmdhd.bin \
    $(VENDOR_FOLDER)/proprietary/system/vendor/firmware/fw_bcmdhd_apsta.bin:system/vendor/firmware/fw_bcmdhd_apsta.bin \
    $(VENDOR_FOLDER)/proprietary/system/vendor/firmware/fw_bcmdhd_mfg.bin:system/vendor/firmware/fw_bcmdhd_mfg.bin

# Codecs
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_aacdec_sprd.so:system/lib/libomx_aacdec_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_apedec_sprd.so:system/lib/libomx_apedec_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_avcdec_hw_sprd.so:system/lib/libomx_avcdec_hw_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_avcdec_sw_sprd.so:system/lib/libomx_avcdec_sw_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_avcenc_hw_sprd.so:system/lib/libomx_avcenc_hw_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_m4vh263dec_hw_sprd.so:system/lib/libomx_m4vh263dec_hw_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_m4vh263dec_sw_sprd.so:system/lib/libomx_m4vh263dec_sw_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_m4vh263enc_hw_sprd.so:system/lib/libomx_m4vh263enc_hw_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_mp3dec_sprd.so:system/lib/libomx_mp3dec_sprd.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libomx_vpxdec_hw_sprd.so:system/lib/libomx_vpxdec_hw_sprd.so

# Stagefright
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefrighthw.so:system/lib/libstagefrighthw.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_aacdec.so:system/lib/libstagefright_sprd_aacdec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_apedec.so:system/lib/libstagefright_sprd_apedec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_h264dec.so:system/lib/libstagefright_sprd_h264dec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_h264enc.so:system/lib/libstagefright_sprd_h264enc.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_mp3dec.so:system/lib/libstagefright_sprd_mp3dec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_mpeg4dec.so:system/lib/libstagefright_sprd_mpeg4dec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_mpeg4enc.so:system/lib/libstagefright_sprd_mpeg4enc.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_soft_h264dec.so:system/lib/libstagefright_sprd_soft_h264dec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_soft_mpeg4dec.so:system/lib/libstagefright_sprd_soft_mpeg4dec.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libstagefright_sprd_vpxdec.so:system/lib/libstagefright_sprd_vpxdec.so

# SPRD scx35 common
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libsprdstreamrecoder.so:system/lib/libsprdstreamrecoder.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/libvtmanager.so:system/lib/libvtmanager.so

# SPRD Res Monitor
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/sprd_res_monitor:system/bin/sprd_res_monitor \
    $(VENDOR_FOLDER)/proprietary/system/etc/monitor.conf:system/etc/monitor.conf \
    $(VENDOR_FOLDER)/proprietary/system/etc/sprd_monitor-user.conf:system/etc/sprd_monitor-user.conf \
    $(VENDOR_FOLDER)/proprietary/system/etc/sprd_monitor-userdebug.conf:system/etc/sprd_monitor-userdebug.conf \
    $(VENDOR_FOLDER)/proprietary/system/lib/liboprofiledaemon.so:system/lib/liboprofiledaemon.so

# Bluetooth
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/lib/libbt-vendor.so:system/lib/libbt-vendor.so \
    $(VENDOR_FOLDER)/proprietary/system/lib/modules/pskey_bt.txt:system/lib/modules/pskey_bt.txt

# Media
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/vendor/media/LMspeed_508.emd:system/vendor/media/LMspeed_508.emd \
    $(VENDOR_FOLDER)/proprietary/system/vendor/media/PFFprec_600.emd:system/vendor/media/PFFprec_600.emd

# Scripts
PRODUCT_COPY_FILES += \
    $(VENDOR_FOLDER)/proprietary/system/bin/ext_data.sh:system/bin/ext_data.sh \
    $(VENDOR_FOLDER)/proprietary/system/bin/ext_kill.sh:system/bin/ext_kill.sh \
    $(VENDOR_FOLDER)/proprietary/system/bin/inputfreq.sh:system/bin/inputfreq.sh \
    $(VENDOR_FOLDER)/proprietary/system/bin/recoveryfreq.sh:system/bin/recoveryfreq.sh \
    $(VENDOR_FOLDER)/proprietary/system/xbin/zram.sh:system/xbin/zram.sh
